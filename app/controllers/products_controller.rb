class ProductsController < ApplicationController

  def create
    @category = Category.find(params[:category_id])
    @product = @category.products.build(product_params)
    if @product.save
      flash[:notice] = 'El producto fue guardado'
    else
      flash[:notice] = 'No se pudo guardar el producto'
    end
    redirect_to @product.category
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    flash[:notice] = 'Producto destruído'
    redirect_to @product.category
  end

  private

  def product_params
    params.require(:product).permit(:name, :price, :category_id)
  end
end
